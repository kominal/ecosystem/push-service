import { ConsumeMessage, ConfirmChannel } from 'amqplib';
import { warn } from '@kominal/lib-node-logging';
import { PushNotificationMessage } from '@kominal/ecosystem-models/pushnotification.message';
import { PushSubscriptionDatabase } from './models/pushsubscription/pushsubscription.database';
import webpush from 'web-push';
import { VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY } from './environment';

webpush.setVapidDetails('mailto:mail@connect.kominal.com', VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY);

export async function onPushNotification(channel: ConfirmChannel, msg: ConsumeMessage | null): Promise<void> {
	if (!msg) {
		warn('Received message without conent.');
		return;
	}

	const { userIds, pushnotification }: PushNotificationMessage = JSON.parse(msg.content.toString());

	for (const userId of userIds) {
		try {
			const notificationPayload = JSON.stringify({
				notification: pushnotification,
			});
			const subscriptions = await PushSubscriptionDatabase.find({ userId });
			for (const subscription of subscriptions) {
				try {
					const result = await webpush.sendNotification(JSON.parse(subscription.get('token')), notificationPayload);
					if (result.statusCode > 299) {
						await PushSubscriptionDatabase.remove(subscription);
					}
				} catch (error) {
					console.log(error);
				}
			}
		} catch (e) {
			console.log(e);
		}
	}

	channel.ack(msg);
}
