import { AESEncrypted } from '@kominal/ecosystem-models/aes.encrypted';

export interface PushSubscription {
	id: string;
	token: string;
	device: AESEncrypted;
	created: number;

	deviceDecrypted: string;
	createdString: string;
}
