import Service from '@kominal/service-util/helper/service';
import register from './routes/register';
import remove from './routes/remove';
import list from './routes/list';
import { onPushNotification } from './handler.rmq';

const service = new Service({
	id: 'push-service',
	name: 'Push Service',
	description: 'Manages pushnotification subscriptions.',
	jsonLimit: '16mb',
	routes: [register, remove, list],
	database: 'push-service',
	rabbitMQ: async (channel) => {
		await channel.assertQueue('ecosystem.pushnotification', { durable: false, messageTtl: 60000 });
		channel.consume('ecosystem.pushnotification', (msg) => onPushNotification(channel, msg));
	},
});
service.start();

export default service;
